from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.shortcuts import redirect
from receipts.models import Account, ExpenseCategory, Receipt

# Create your views here.


class ReceiptCreateView(LoginRequiredMixin,CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor","total","tax","date","purchaser","category","account"]
    success_url = reverse_lazy("receipts_list")

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("receipts_list")

class ReceiptListView(LoginRequiredMixin,TemplateView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Model.objects.filter(receipts=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expenses/create.html"
    fields = ["name","owner"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner= self.request.user
        item.save()
        return redirect("expenses_list")

class ExpenseCategoryListView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expenses/list.html"
    fields = ["name","owner"]

    def get_queryset(self):
        return Model.objects.filter(categories=self.request.user)

class AccountCreateView(LoginRequiredMixin,CreateView):
    model = Account
    template_name = "account/create.html"
    fields = ["name","number","owner"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("account_list")

class AccountListView(LoginRequiredMixin,CreateView):
    model = Account
    template_name = "account/list.html"
    fields = ["name","number","owner"]

    def get_queryset(self):
        return Model.objects.filter(accounts=self.request.user)