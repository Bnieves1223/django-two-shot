from django.urls import path
from receipts.views import ReceiptListView, ReceiptCreateView, ExpenseCategoryCreateView, ExpenseCategoryListView, AccountCreateView, AccountListView



urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("account/create", AccountCreateView.as_view(), name="account_create"),
    path("categories/create", ExpenseCategoryCreateView.as_view(), name="expenses_create"),
    path("create/", ReceiptCreateView.as_view(),name="receipts_create"),
    path("categories/",ExpenseCategoryListView.as_view(),name ="expenses_list"),
    path("accounts/",AccountListView.as_view(),name="account_list"),
    path("",ReceiptListView.as_view(),name = "receipts_list"),







]
