from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect

# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password1')
            user = authenticate(username=username, password=password)
            user.save()
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm(request.POST)
    context = {
        "form": form,
    }

    return render(request, 'registration/signup.html', context)